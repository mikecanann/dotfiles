


# export NODEJS_HOME=$HOME/bin/nodejs/node-v10.19.0-darwin-x64
# export NODEJS_HOME=$HOME/bin/nodejs/node-v10.22.1-darwin-x64
# export NODEJS_HOME=$HOME/bin/nodejs/node-v14.16.1-darwin-x64
export NODEJS_HOME=$HOME/bin/nodejs/node-v14.17.3-darwin-x64

export PATH=$NODEJS_HOME/bin:$PATH

export YARN_HOME=$HOME/bin/yarn/yarn-v1.22.4
export PATH=$YARN_HOME/bin:$PATH

export PATH=/Applications/MacVim.app/Contents/bin:$PATH

# brew install shared-mime-info
export FREEDESKTOP_MIME_TYPES_PATH=/usr/local/Cellar/shared-mime-info/2.1/share/shared-mime-info/packages/freedesktop.org.xml

################################################################################
# Proxies and env vars for when working on VPN
################################################################################
proxy_set() {
  echo "Setting proxies"
  export PROXY_URL="http://preproxy.uscis.dhs.gov:80"
  export HTTPS_PROXY="${PROXY_URL}"
  export https_proxy="${PROXY_URL}"
  export HTTP_PROXY="${PROXY_URL}"
  export http_proxy="${PROXY_URL}"
  export NO_PROXY="localhost,127.0.0.1,.dhs.gov,dhs.gov,nexus-nonprod-gss.uscis.dhs.gov,nexus-gss.uscis.dhs.gov"
  export no_proxy="${NO_PROXY}"
  export ALL_PROXY="https://el2-dt-preproxy.uscis.dhs.gov:80/"
  export all_proxy="https://el2-dt-preproxy.uscis.dhs.gov:80/"
  export GRADLE_OPTS="-noverify -Dhttp.proxyHost=preproxy.uscis.dhs.gov -Dhttp.proxyPort=80"
  export GRADLE_OPTS="$GRADLE_OPTS -Dhttps.proxyHost=preproxy.uscis.dhs.gov -Dhttps.proxyPort=80"
  export GRADLE_OPTS="$GRADLE_OPTS -Dhttp.nonProxyHosts='localhost|nexus-nonprod-gss.uscis.dhs.gov|nexus-gss.uscis.dhs.gov'"

  npm config set registry="https://nexus-gss.uscis.dhs.gov/nexus/repository/didit-npm-group/"
  yarn config set registry "https://nexus-gss.uscis.dhs.gov/nexus/repository/didit-npm-group/"
}

# Immediately call it
proxy_set

# When off VPN
proxy_unset() {
  echo "Unsetting proxies"
  unset PROXY_URL
  unset HTTPS_PROXY
  unset https_proxy
  unset HTTP_PROXY
  unset http_proxy
  unset NO_PROXY
  unset no_proxy
  unset ALL_PROXY
  unset all_proxy
  unset GRADLE_OPTS
  
  npm config set registry="https://registry.npmjs.org"
  yarn config set registry "https://registry.npmjs.org"
}



# Force close the VPN.  @TODO: "Put script inline"
function force_close_vpn {
    [[ -s "$HOME/code/fqVPN.scpt" ]] && osascript "$HOME/code/fqVPN.scpt"
}
# Disconnect VPN and proxy_unset
function vpn_off {
    /opt/cisco/anyconnect/bin/vpn disconnect
    proxy_unset
}
# Connect to VPN and proxy_set
function vpn_on {
    #TODO: Check if VPN is on
    proxy_set
    /opt/cisco/anyconnect/bin/vpn connect CIS_PIV_Mercury3_HA
}
# Kill the VPN agent daemon
function kill_cisco {
    sudo kill -9 $(ps -ef | grep vpnagentd | head -1 | awk '{ print $2; }')
}
################
# Certificates #
################
# rebuild openssl pems using keychains. I stole the guts of this script from https://github.com/raggi/openssl-osx-ca
# which would have been great except that it only exported root CA certs to the openssl pem file. Unfortunately,
# a lot of our https sites are not exporting the entire cert chain for the https cert. That causes issues when
# the client doesn't have all of the intermediate certs. This script will export all of the certs in the targeted
# keychains, both root CA and intermediate certs.
function _genbundle() {
    local sslimpl=$1
    local fresh_cert_bundle=$2
    local list=$(brew list $sslimpl 2>/dev/null)
    [[ -z $list ]] && continue
    local openssl=$(echo "$list" | grep bin/openssl | head -n 1)
    # because of the way bash works - these error conditions will not be trapped by the rebuild_pem_files function
    [[ "${openssl}" = "" ]] && echo "Homebrew $sslimpl not found" && return 1
    local openssldir=$($openssl version -d | cut -d '"' -f 2)
    [[ "${openssldir}" = "" ]] && echo "$sslimpl directory not found" && return 1
    d1=$($openssl md5 ${openssldir}/cert.pem | awk '{print $2}')
    d2=$($openssl md5 ${fresh_cert_bundle}| awk '{print $2}')
    if [[ "${d1}" = "${d2}" ]]; then
     echo "${openssldir}/cert.pem up to date"
    else
        # XXX: I don't think this is atomic on OSX, but it's as close as we're going to
        # get without a lot more work.
        cp -f ${fresh_cert_bundle} ${openssldir}/cert.pem
        echo "${openssldir}/cert.pem updated"
    fi
}
# this will rebuild the PEM based CA cert bundle used by any openssl or libressl installed by brew
function rebuild_pem_files() {
    local brew=$(which brew)
    if [[ ! -x "${brew}" ]]; then
        echo "Homebrew not in PATH, cannot continue"
        exit 1
    fi
    local temp_cert_bundle=$(/usr/bin/mktemp -t fresh_cert_bundle.pem)
    [[ "${temp_cert_bundle}" = "" ]] && echo "mktemp failed" && echo ${temp_cert_bundle} && return 1
    echo 'Outputting certs from System.keychain'
    security find-certificate -a -p /Library/Keychains/System.keychain > ${temp_cert_bundle}
    echo 'Outputting certs from SystemRootCertificates.keychain'
    security find-certificate -a -p /System/Library/Keychains/SystemRootCertificates.keychain >> ${temp_cert_bundle}
    local impls="$(brew list | grep -E '^((libre|open)ssl(@[0-9\.]+)?)$')"
    for sslimpl in ${impls}; do
        _genbundle $sslimpl $temp_cert_bundle
    done
    # clean up the temp file
    rm ${temp_cert_bundle}
}
# ICAM SSO Login to App Servers (Preview, Staging & Production)
function add_ICAM_SSO {
    defaults write /Users/$USER/Library/Preferences/com.google.Chrome.plist AuthServerWhitelist sso.dhs.gov,sso.uscis.dhs.gov,sso.preprod.uscis.dhs.gov,login.uscis.dhs.gov,login-preprod.uscis.dhs.gov,login-test.uscis.dhs.gov,login-dev.uscis.dhs.gov
    defaults write /Users/$USER/Library/Preferences/com.google.Chrome.plist AuthNegotiateDelegateWhitelist sso.dhs.gov,sso.uscis.dhs.gov,sso.preprod.uscis.dhs.gov,login.uscis.dhs.gov,login-preprod.uscis.dhs.gov,login-test.uscis.dhs.gov,login-dev.uscis.dhs.gov
}
# KeyChain Access won't accept password
function fix_keychain_pwd {
    rm -rf ~?.Library/Keychains/{.fl*,*}
}
# Forwarding Smartcard keys
function add_keys {
    pkill ssh-agent
    #ssh-add Any-other-keys-you-have
    ssh-add -s /usr/local/share/centrifydc/lib/pkcs11/tokendPKCS11.so
}
# Find your Smarcard SSH public key
function find_PIV_SSH_public_key {
    ssh-keygen -i -m pkcs8 -f <(security find-certificate -p | openssl x509 -noout -pubkey)
}


update_PS1 () {
  export PS1="[\[\e[m\]\[\e[36m\]\u\[\e[m\] \[\e[31m\]\w\[\e[m\]\e[0;32m$(__git_ps1)\e[m]$ "
}
PROMPT_COMMAND=update_PS1




if [ -f ~/git-completion.bash ]; then
    . ~/git-completion.bash
fi

# source /Applications/Xcode.app/Contents/Developer/usr/share/git-core/git-prompt.sh
source ~/.git-prompt.sh


source $HOME/.rvm/scripts/rvm

