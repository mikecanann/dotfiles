set encoding=utf-8

set colorcolumn=80
" ================ Grep Settings ====================

" == grep settings and shortcuts
set grepprg=grep\ -n\ -r\ --exclude=package-lock.json\ --exclude=yarn.lock\ --exclude=tags\ --exclude=tags.temp\ --exclude-dir=coverage\ --exclude-dir=\*.git\*\ --exclude-dir=\*dist\*\ --exclude-dir=\*node_modules\*\ $*

" for imgrep. ex:
" :vimgrep /search_term/g **
" :copen
set wildignore+=**/bower_components/**,**/node_modules/**,**vendor/bundle**,**.git**,**/dist/**


" ===================================================
" ================ Key Mappings =====================
" ===================================================

" insert the current date
imap <Leader>day   <C-R>=strftime("%Y%m%d %H:%M")<CR>


" ================ vim-node-inspect =================
" tried to match up with Visual Studio
nnoremap <silent><F4>    :NodeInspectStart<cr>
nnoremap <silent><F5>    :NodeInspectRun<cr>
nnoremap <silent><S-F5>  :NodeInspectStop<cr>
nnoremap <silent><F7>    :NodeInspectRemoveAllBreakpoints<cr>
nnoremap <silent><F9>    :NodeInspectToggleBreakpoint<cr>
nnoremap <silent><F10>   :NodeInspectStepOver<cr>
nnoremap <silent><F11>   :NodeInspectStepInto<cr>
nnoremap <silent><S-F11> :NodeInspectStepOut<cr>
nnoremap <silent><F12>   :NodeInspectConnect("127.0.0.1:9229")<cr>

" remap '*' to 'g*' (Search for text without word boundry)
" map <silent> * g*

nnoremap <silent> <leader>y  :<C-u>CocList -A --normal yank<cr>

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

map <C-Tab> gt
map <C-S-Tab> gT

" Maps Alt-[h,j,k,l] to resizing a window split
noremap <C-Left> <C-w><
noremap <C-Down> <C-W>-
noremap <C-Up> <C-W>+
noremap <C-Right> <C-w>>

command! Q q " Bind :Q to :q
command! W w " Bind :W to :w

" allow the . to execute once for each line of a visual selection
vnoremap . :normal .<CR>


" insert mode only, remap jk to esc
inoremap jk <esc>

" to change window sizes:
"  type ^W++- instead of ^W+^W+^W-
nmap          <C-W>+     <C-W>+<SID>ws
nmap          <C-W>-     <C-W>-<SID>ws
nn <script>   <SID>ws+   <C-W>+<SID>ws
nn <script>   <SID>ws-   <C-W>-<SID>ws
nmap          <SID>ws    <Nop>

" to change window sizes:
"  type ^W>>< instead of ^W>^W>^W<
nmap          <C-W>>     <C-W>><SID>ws
nmap          <C-W><     <C-W><<SID>ws
nn <script>   <SID>ws>   <C-W>><SID>ws
nn <script>   <SID>ws<   <C-W><<SID>ws
nmap          <SID>ws    <Nop>



" close current file with \q
noremap <silent> <Leader>q :q<CR>
inoremap <silent> <Leader>q <c-o>:q<CR>
" save file with \w
noremap <silent> <Leader>w :w<CR>
inoremap <silent> <Leader>w <c-o>:w<CR>
" close a window/split (will not exit if it is the last window open)
noremap <silent> <Leader>c :close<CR>
inoremap <silent> <Leader>c <c-o>:close<CR>


let $FZF_DEFAULT_COMMAND='find . \( -name node_modules -o -name .git \) -prune -o -print'
nnoremap <silent> <Leader><SPACE> :Files!<CR>


" grep for word under the cursor
nnoremap <Leader>vv :silent grep! <cword> * <CR><CR>:cw<CR>

" prompt for the string to grep
command! -nargs=+  -complete=file -bar Grep grep! <args> * |cw
nnoremap <Leader>vp :silent Grep<space>-i<space>

set switchbuf+=usetab,vsplit

" quick switch to terminal (and same keystroke to close it)
noremap <C-d> :terminal<cr>


" open a new tab
noremap <Leader>n :tabnew<cr>

" easymotion jump any direction to a char
nmap s <Plug>(easymotion-overwin-f)
nmap S <Plug>(easymotion-overwin-f)

" use the 'standard' keys for cut, copy and paste
" using the system clipboard (*)
vmap <C-c> "*yi
vmap <C-x> "*c
vmap <C-v> c<ESC>"*p
imap <C-v> <C-r><C-o>*

" in terminal mode, hold down the shift to get terminal copy/paste menu

" delete, and don't place in the clipboard, delete to /dev/null
noremap <Leader>d "_d


" for csv.vim - use all rows when using the commands AttangeColumn and UnArrangeColumn
let b:csv_arrange_use_all_rows = 1

" Bind this for opening the explorer
nmap <leader>e :CocCommand explorer<CR>

nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>- <Plug>AirlineSelectPrevTab
nmap <leader>+ <Plug>AirlineSelectNextTab

nnoremap <silent> <leader><bar> :call ToggleIndentGuides()<cr>

nnoremap <leader>ll :call ToggleOverLengthHighlight()<CR>


" Use <c-space> or ,c-@ to trigger completion.
if has('nvim')
  inoremap <silent><expr> <C-space> coc#refresh()
else
  inoremap <silent><expr> <C-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>


" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)


" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" ================ CocList ==========================
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" append result on current expression
nmap <Leader>ca <Plug>(coc-calc-result-append)
" replace result on current expression
nmap <Leader>cr <Plug>(coc-calc-result-replace)

" ================ terminal commands  ===============
" to enter terminal normal mode. use "i" to exit
tnoremap <Esc><Esc> <C-\><C-n>
tnoremap <C-v> <C-w>"*



" Increase the timeout to 2 seconds. The default is 1000 miliseconds.
set timeoutlen=2000
set ttimeoutlen=2000

" ================ Plugin ===========================
filetype plugin on " enable plugins
filetype plugin indent on

" ================ Bell =============================
set belloff=all


" ================ Folds ============================
set foldmethod=indent
set foldlevelstart=99

" ================ Indentation ======================
set shiftwidth=2  " width of autoindent
set autoindent    " turn on autoindent
set tabstop=8     " number of spaces a tab counts for
set softtabstop=2
set shiftwidth=2
set expandtab     " turn a tab into 2 spaces

" set paste " turn off autoindent on paste
set pastetoggle=<F2>

augroup tabspacing
  " change tab to 4 characters for java
  autocmd Filetype java setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
  autocmd Filetype jsp setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
  autocmd Filetype json setlocal ts=2 sw=2 expandtab
augroup end


" ================ misc =============================


" disable ale's lsp, so coc.nvim can use Language Server Protocol (lsp)
let g:ale_disable_lsp = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'
let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '⬥ ok']


" ================ Plug =============================
" install vim-plug - github.com/junegunn/vim-plug
"   put plug.vim in the 'autoload' directory
"
" install
"    :PlugInstall
"
" verify
"    :PlugStatus
call plug#begin('~/.vim/plugged')

  " on windows
  "   copy ~/.vim/autoload/plug.vim to ~/vimfiles/autoload/.
  "   copy ~/.vim/coc-settings.json to ~/vimfiles/.

  " needs yarn
  " needs node
  " needs go

  Plug 'https://github.com/tomtom/tcomment_vim.git'

  " switched from w0rp to dense-analysis"
  " Plug 'https://github.com/w0rp/ale.git'
  Plug 'https://github.com/dense-analysis/ale.git'

  Plug 'https://github.com/chrisbra/csv.vim.git' " mrc add menu

  " <leader><space> - to fuzzy find files
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

  Plug 'https://github.com/vim-airline/vim-airline.git'
  Plug 'https://github.com/vim-airline/vim-airline-themes.git'

  Plug 'https://github.com/gko/vim-coloresque.git'

  Plug 'https://github.com/easymotion/vim-easymotion.git' " mrc add menu?

  Plug 'https://github.com/tpope/vim-fugitive.git' " mrc add menu?
  Plug 'https://github.com/airblade/vim-gitgutter.git'

  Plug 'https://github.com/luochen1990/rainbow'
  "  sudo apt install git-lfs
  "  when active, not working with jsx indenting
  " Plug 'https://github.com/krischik/vim-rainbow', {'branch': 'master'}

  Plug 'https://github.com/sheerun/vim-polyglot'

  " Plug 'https://github.com/dunstontc/vim-code-dark.git'
  Plug 'https://github.com/tomasiser/vim-code-dark.git'


  " <leader>ww on both windows to swap
  Plug 'https://github.com/wesQ3/vim-windowswap'
  " must be after other plugins that this is adding icons to
  Plug 'https://github.com/ryanoasis/vim-devicons'

  " syntax highlighting for styled-components css
  Plug 'https://github.com/styled-components/vim-styled-components', { 'branch': 'main' }

  " snippits press <c-y>, (Ctrl-y then comma) to expand
  " https://docs.emmet.io/cheat-sheet/
  Plug 'https://github.com/mattn/emmet-vim' " mrc add some menu options

  Plug 'https://github.com/neoclide/coc.nvim', {'branch': 'release'}

  " enable this if switching to C/C++, needs clangd installed
  " Plug 'https://github.com/clangd/coc-clangd', , {'branch': 'release', 'do': 'yarn install --frozen-lockfile'}
  Plug 'https://github.com/neoclide/coc-eslint', {'do': 'yarn install --frozen-lockfile'}

  Plug 'https://github.com/weirongxu/coc-explorer', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

  Plug 'https://github.com/neoclide/coc-html', {'do': 'yarn install --frozen-lockfile'}
  Plug 'https://github.com/neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}

  Plug 'https://github.com/neoclide/coc-yank', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

  Plug 'https://github.com/neoclide/coc-css', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

  " inserts pair characters automatically
  Plug 'https://github.com/neoclide/coc-pairs', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}


  Plug 'https://github.com/weirongxu/coc-calc', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}

  " requires universal-ctags to be installed - uses the tags file
  "   https://github.com/universal-ctags/ctags

  Plug 'https://github.com/ludovicchabant/vim-gutentags.git'

  " :GV - to see the git logs
  Plug 'https://github.com/junegunn/gv.vim'

  " go get -v golang.org/x/tools/gopls
  Plug 'https://github.com/fatih/vim-go', { 'do': ':GoUpdateBinaries' }

  " node debuggr
  Plug 'https://github.com/eliba2/vim-node-inspect'

  " rust find the commands for ft_rust and coc-rust-analyzer
  " not using rust right now
  " Plug 'https://github.com/fannheyward/coc-rust-analyzer', {'do': 'yarn install --frozen-lockfile'}

  " pip3 install --user git+https://github.com/ryanpetrello/vimwhisperer.git
  " mrc - not quite working
  Plug 'https://github.com/ryanpetrello/vimwhisperer'

  " open menu with 'space space'
  Plug 'https://github.com/skywind3000/vim-quickui'


call plug#end()

" ================ search ===========================
set ignorecase " set search to be not case-sensitive
set smartcase  " use case, when using upper case letters
set incsearch  " show matches while typing search pattern
set hlsearch   " highlight the last search



" ================ Scrolling ========================
set scrolloff=6 "Start scrolling when we're 6 lines away from top/bottom

" ================ General Config ====================
set backspace=indent,eol,start " allow backspace over autoindent, etc
set whichwrap=<,>,h,l,[,] " allow cursor to move to previous and next line
set ruler           " show the line and column number
set laststatus=2    " always display status bar
set history=1000    " store lots of :cmdline history
set showcmd         " show incomplete cmds down the bottom
set showmode        " Show current mode down the bottom
set number          " display line numbers on the left of the screen

" ================ encryption =======================
setlocal cm=blowfish2 " change the cryptmethod default encryption to blowfish2


" ================ Completion =======================

set wildmenu  " enable ctrl-n and ctrl-p (and tab) to scroll thru matches

" have completion behave similarly to a shell (complete only up to point of ambiguity)
set wildmode=list:longest

set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif



" ================ Turn Off Swap Files ==============
set noswapfile    " use memory instead of disk for buffers
set nobackup      " no backup of old file
set nowritebackup " do not make a backup before overwriting a file

" ================ Tab Title (# and modified flag) ==
set guitablabel=%N\ %t%M


" ================ window settings - add screen space
" remove the menu bar
" set guioptions-=m
" remove toolbar
" set guioptions-=T




" ================ clipboard ========================
" cut and paste use the system clipboard - doesn't require prefixing "*
if  has("unix")  || has("macunix") || has("win32unix")
  set clipboard=unnamedplus
else
  set clipboard=unnamed
endif

let s:uname = system("uname -s")
if s:uname == "Darwin\n"
  " using latest mac
  set clipboard=unnamed
endif

" ================ rainbow =====================================

let g:rainbow_active = 1


set showmatch

" ================ Combining Characters ========================
"  modify selected text using combining diacritics
command! -range -nargs=0 Overline        call s:CombineSelection(<line1>, <line2>, '0305')
command! -range -nargs=0 Underline       call s:CombineSelection(<line1>, <line2>, '0332')
command! -range -nargs=0 DoubleUnderline call s:CombineSelection(<line1>, <line2>, '0333')
command! -range -nargs=0 Strikethrough   call s:CombineSelection(<line1>, <line2>, '0336')

function! s:CombineSelection(line1, line2, cp)
  execute 'let char = "\u'.a:cp.'"'
  execute a:line1.','.a:line2.'s/\%V[^[:cntrl:]]/&'.char.'/ge'
endfunction




" delcombine will allow the combining character to be deleted separately
set delcombine


" ================ coc-explorer ================================
function! s:explorer_cur_dir()
  let node_info = CocAction('runCommand', 'explorer.getNodeInfo', 0)
  return fnamemodify(node_info['fullpath'], ':h')
endfunction

function! s:exec_cur_dir(cmd)
  let dir = s:explorer_cur_dir()
  execute 'cd ' . dir
  execute a:cmd
endfunction

function! s:init_explorer()
  "set winblend=10

  " Integration with other plugins

  " CocList
  nmap <buffer> <Leader>fg <Cmd>call <SID>exec_cur_dir('CocList -I grep')<CR>
  nmap <buffer> <Leader>fG <Cmd>call <SID>exec_cur_dir('CocList -I grep -regex')<CR>
  nmap <buffer> <C-p> <Cmd>call <SID>exec_cur_dir('CocList files')<CR>

  " vim-floaterm
  nmap <buffer> <Leader>ft <Cmd>call <SID>exec_cur_dir('FloatermNew --wintype=floating')<CR>
endfunction

function! s:enter_explorer()
  if &filetype == 'coc-explorer'
    " statusline
    setl statusline=coc-explorer
  endif
endfunction

augroup CocExplorerCustom
  autocmd!
  autocmd BufEnter * call <SID>enter_explorer()
  autocmd FileType coc-explorer call <SID>init_explorer()
augroup END


" ================ Tabs ========================================
" using airline to tabs (excluding buffer display)

let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_tab_type = 0
let g:airline#extensions#tabline#show_close_button = 0

let g:airline#extensions#tabline#tab_nr_type = 1 " 0 = # or splits, 1 = tab number, 2 splits and tab number
let g:airline#extensions#tabline#show_tab_nr = 1

let g:airline#extensions#tabline#buffer_idx_mode = 1

" ================ Tags ========================================
" search back and find tags in parent folders, starting in the
" directory of the current file
" :set tags=./tags;


" ================ Mouse =======================================
" enable mouse for all modes
:set mouse=a

" ================ colors ===========================
" Must be after plug#end()
set background=dark
" colorscheme torte
" colorscheme monokai
" colorscheme codedark
"colorscheme moonfly " has to be installed by Plug to work
" let g:airline_theme = 'codedark'
"let g:airline_theme = 'moonfly'

colorscheme codedark " has to be installed by Plug to work
let g:airline_theme = 'codedark'


" set to make the terminal colors more closly match the gvim colors
" ( makes some brighter)
set termguicolors


"
function! ToggleIndentGuides()
  if !exists('b:indentguides')
    if !&expandtab && &tabstop == &shiftwidth
      let b:indentguides = 'tabs'
      let b:indentguides_listopt = &l:'Cascursive', 'Droid Sans Mono', 'monospace', monospacelist
      let b:indentguides_listcharsopt = &l:listchars
      exe 'setl listchars' . '+'[!&l:list] . '=tab:˙\  list'
    else
      let b:indentguides = 'spaces'
      let pos = range(1, &textwidth > 0 ? &textwidth : 80, &shiftwidth)
      call map(pos, '"\\%" . v:val . "v"')
      let pat = '\%(\_^ *\)\@<=\%(' . join(pos, '\|') . '\) '
      let b:indentguides_match = matchadd('ColorColumn', pat)
    endif
  else
    if b:indentguides == 'tabs'
      let &l:list = b:indentguides_listopt
      let &l:listchars = b:indentguides_listcharsopt
      unlet b:indentguides_listopt b:indentguides_listcharsopt
    else
      call matchdelete(b:indentguides_match)
      unlet b:indentguides_match
    endif
    unlet b:indentguides
  endif
endfunction


" set the main window background foreground colors
" highlight Normal guibg=#222222 guifg=White

" highlight the current line number
" set cursorline

highlight clear SignColumn " for gitgutter use same settings as the line numbers


" ================ Font =============================


if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

let g:airline_powerline_fonts=1
"let g:airline_linecolumn_prefix = '¶' " overwriting airline symbol
let g:airline_symbols.linenr = '¶' " overwriting airline symbol
"let g:airline_whitespace_symbol = '·' " overwriting airline symbol
let g:airline#extensions#whitespace#symbol = '·' " overwriting airline symbol

"let g:airline_section_b = '' " don't display the branch (to free up space)
let g:airline#extensions#branch#displayed_head_limit = 10 " limit branch names to this length

" other good options: base16_chalk cool dark_minimal onedark base16_flat
" let g:airline_theme='base16_shapeshifter'
let g:airline_theme='dark_minimal'

let g:airline_mode_map = {
    \ '__' : '-',
    \ 'c'  : 'C',
    \ 'i'  : 'I',
    \ 'ic' : 'I',
    \ 'ix' : 'I',
    \ 'n'  : 'N',
    \ 'multi'  : 'M',
    \ 'ni' : 'N',
    \ 'no' : 'N',
    \ 'R'  : 'R',
    \ 'Rv' : 'R',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ 't'  : 'T',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ }

let g:airline#extensions#tabline#enabled = 1


if  has("unix")  || has("macunix") || has("win32unix")
  " set guifont=Ubuntu\ Mono\ derivative\ Powerline\ 9
  " set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 8
  " set guifont=Input\ 8
  " set guifont=DejaVuSansMono\ Nerd\ Font\ 9 " doesn't work for devicons
  " from https://github.com/ryanoasis/nerd-fonts


  " working nicely
  "set guifont=MesloLGS\ Nerd\ Font\ Mono\ 10


  " adds ligatures (install all three Cascursive fonts)
  "set guifont=Cascursive\ 10

  " from https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/CascadiaCode
  set guifont=CaskaydiaCove\ Nerd\ Font\ Mono\ 11
endif


if s:uname == "Darwin\n"
  " manually downloaded and installed
  set guifont=DejaVu\ Sans\ Mono\ for\ Powerline:h10
  " set guifont=Cascursive\ 10
  set guifont=Cascursive-Regular:h11

  " from https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/CascadiaCode
  set guifont=CaskaydiaCove\ Nerd\ Font\ Mono:h11
endif

set shell=bash
if  has("win32") || has("win64")
   "installed from: https://github.com/Lokaltog/powerline-fonts/tree/master/UbuntuMono
   "set guifont=Ubuntu_Mono_derivative_Powerlin:h9:cANSI " pretty good
   "installed from: https://github.com/Lokaltog/powerline-fonts/tree/master/DejaVuSansMono
   " set guifont=DejaVu_Sans_Mono_for_Powerline:h9:cANSI " better and unicode

   " this block it to fix a tmp file rights issue
   set shell=cmd.exe
   set shellcmdflag=/c
   let $TMP="c:/tmp"
   let $TEMP="c:/tmp"
   set directory=.,$TMP,$TEMP

   set guifont=CaskaydiaCove_NFM:h9:cANSI
endif


" ================ special char color ===============
" ( needs to be after selecting the colorscheme )

" trailing whitespace, display tabs, hidden whitespace and end of line as dark gray
" set list listchars=trail:¬,tab:»·,nbsp:_,eol:·
" set list listchars=tab:»·,nbsp:_


" SpecialKey nbsp, tab and trail.
highlight SpecialKey ctermfg=59 guifg=#151515
" NonText highlighting will be used for eol (also extends and precedes)
highlight NonText ctermfg=59 guifg=#151515

" highlight text over 100 char ---- mrc - you can only have one 'match' and
" seems like a plugin is using it
"
"highlight OverLength ctermbg=red ctermfg=white guibg=#797979
"match OverLength /\%101v./
" match ErrorMsg '\%101v.'
"autocmd BufWinEnter,BufRead * match OverLength /\%81v./

let g:overlength_enabled = 0
highlight OverLength ctermbg=black guibg=#212121

function! ToggleOverLengthHighlight()
  if g:overlength_enabled == 0
    match OverLength /\%100v.*/
    let g:overlength_enabled = 1
    echo 'OverLength highlighting turned on'
  else
    match
    let g:overlength_enabled = 0
    echo 'OverLength highlighting turned off'
  endif
endfunction

" ================ Set iskeyword to have word movement commands stop at . chars
set iskeyword=65-90,95,97-122,48-57 "the same: a-z,_,A-Z,0-9

augroup scsskeywords
  autocmd FileType scss setl iskeyword+=@-@
augroup end
" ================ Set iskeyword for javascript function name search (don't look for "this.")
setlocal iskeyword-=.

" open new split panes to the right and bottom (more natural than default)
set splitbelow
set splitright

" save whenever switching windows or leaving vim
au FocusLost,WinLeave * :silent! wa
" trigger reload the window when gaining focus
" au FocusGained,BufEnter * :silent! !
au FocusGained,BufEnter * :silent! :checktime




" ================  coc.nvim settings ===================================
"
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction


function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
" augroup coccursor
"   autocmd CursorHold * silent call CocActionAsync('highlight')
" augroup END


augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end


" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}


" turn off autocompletion for text files
augroup coctextfile
  autocmd FileType text let b:coc_suggest_disable = 1
augroup end



" must be after filetype plugin (so moved to the end of the file)
syntax on "turn on syntax highlighting



set guiligatures=!\"#$%&()*+-./:<=>?@[]^_{\|~
" set guiligatures=\ !\"#$%&()*+-./:<=>?@ABCDEFGHIKLMNOPRSTUVWX[]^_{\|~

highlight Comment cterm=italic gui=italic
highlight Statement cterm=italic gui=italic
highlight Conditional cterm=italic gui=italic
highlight Repeat cterm=italic gui=italic
highlight Keyword cterm=italic gui=italic
highlight Exception cterm=italic gui=italic
highlight Type cterm=italic gui=italic
highlight Label cterm=italic gui=italic
highlight Debug cterm=italic gui=italic
highlight StorageClass cterm=italic gui=italic
highlight Structure cterm=italic gui=italic
highlight Typedef cterm=italic gui=italic


" must be after colorscheme settings
highlight colorcolumn ctermbg=grey guibg=#282828





if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window (for an alternative on Windows, see simalt below).
  " set lines=45 columns=250
endif


" ================ set coc windows and split ========
" this should be after 'splitright'

autocmd User CocNvimInit :CocCommand explorer

":vnew

" close the window if the only one left is explore
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif


" ================ ==================================

highlight Normal guibg=black
highlight CursorLine guibg=#550000


function! TermExit(code)
	echom "terminal exit code: ". a:code
	echom "current win: ". winnr()
endfunc
let g:opts = {'w':80, 'h':24, 'callback':'TermExit'}
let g:opts.title = 'Terminal Popup'


let g:quickui_color_scheme = 'papercol dark'

" clear all the menus
call quickui#menu#reset()


" register HELP menu with weight 1
call quickui#menu#install('File', [
  \ ["Fuzzy Find Files", 'Files!', 'leader space'],
  \ ], 1)

call quickui#menu#install('Git', [
  \ ["Git Logs", 'GV', ':GV'],
  \ ["Git Logs current file", 'GV!', ':GV!'],
  \ ["Git Logs revisions", 'GV?', ':GV?'],
  \ ], 10)


call quickui#menu#install('Grep', [
  \ ["Grep word under cursor", 'exec "silent grep! "  . expand("<cword>") . " * \n\n:cw\n " ', 'leader vv'],
  \ ["prompt for string to Grep", 'exec "silent Grep -i " . quickui#input#open("Enter search term:", "") ', 'leader vp'],
  \ ], 20)

call quickui#menu#install('Nav', [
  \ ["diagnostic prev", 'call feedkeys("[g\n")', '[g'],
  \ ["diagnostic next", 'call feedkeys("]g\n")', ']g'],
  \ ['--',''],
  \ ["coc definition", 'call feedkeys("gd\n")', 'gd'],
  \ ["coc type definition", 'call feedkeys("gy\n")', 'gy'],
  \ ["coc implementation", 'call feedkeys("gi\n")', 'gi'],
  \ ["coc references", 'call feedkeys("gr\n")', 'gr'],
  \ ['--',''],
  \ ["Show Documentation", 'call feedkeys("K\n")', 'K'],
  \ ], 30)


call quickui#menu#install('Misc', [
  \ ["insert day", 'call feedkeys("i\\day")', 'leader day'],
  \ ['--',''],
  \ ["remove highlight", 'call feedkeys(":noh\n")', 'leader <cr>'],
  \ ['--',''],
  \ ["Rename Symbol", 'call feedkeys("\\rn\n")', 'leader rn'],
  \ ['--',''],
  \ ["toggle indent guides", 'call feedkeys("\\\|\n")', 'leader |'],
  \ ['--',''],
  \ ["float terminal", 'call quickui#terminal#open("bash", g:opts)', 'ctrl d'],
  \ ['--',''],
  \ ["view yanks", 'call feedkeys("\\y\n")', 'leader y'],
  \ ['--',''],
  \ ["calculate append", 'call feedkeys("\\ca\n")', 'leader ca'],
  \ ["calculate replace", 'call feedkeys("\\cr\n")', 'leader cr'],
  \ ['--',''],
  \ ["tag goto", 'exec "tag " . expand("<cword>")', 'ctrl ]'],
  \ ["tag pop", 'pop', 'ctrl t'],
  \ ], 40)

call quickui#menu#install('Node', [
  \ ["Node Inspect Start", 'NodeInspectStart', '<F4>'],
  \ ["Node Inspect Run", 'NodeInspectRun', '<F5>'],
  \ ["Node Inspect Stop", 'NodeInspectStop', '<S-F5>'],
  \ ["Node Inspect Remove All Breakpoints", 'NodeInspectRemoveAllBreakpoints', '<F7>'],
  \ ["Node Inspect Toggle Breakpoint", 'NodeInspectToggleBreakpoint', '<F9>'],
  \ ["Node Inspect Step Over", 'NodeInspectStepOver', '<F10>'],
  \ ["Node Inspect Step Into", 'NodeInspectStepInto', '<F11>'],
  \ ["Node Inspect Step Out", 'NodeInspectStepOut', '<S-F11>'],
  \ ["Node Inspect Connect", 'NodeInspectConnect("127.0.0.1:9229")', '<S-F11>'],
  \ ], 50)

call quickui#menu#install('comments', [
  \ ["Toggle Comments", 'call feedkeys("gcc\n")', 'gcc (line) gc (visual mode)'],
  \ ], 60)

call quickui#menu#install('csv', [
  \ ["What Column", 'WhatColumn', ':WhatColumn'],
  \ ["Nr Columns", 'NrColumns', ':NrColumns'],
  \ ["Search In Column", 'exec "SearchInColumn " . quickui#input#open("column number:", "") . " /" . quickui#input#open("search term:", "") . "/" ', ':SearchInColumn # /text/'],
  \ ["Highlight Columns", 'exec "HiColumn " . quickui#input#open("column number:", "")', ':NrColumns'],
  \ ["Highlight remove", 'exec "HiColumn!"', ':NrColumns'],
  \ ["ArrangeColumn", 'call feedkeys("gg\nV\nG\n:ArrangeColumn\n")', ':ArrangeColumn (visual mode use range)'],
  \ ["UnArrangeColumn", 'call feedkeys("gg\nV\nG\n:UnArrangeColumn\n")', ':UnArrangeColumn (visual mode use range)'],
  \ ["Sort by column", 'exec feedkeys("gg\nV\nG\n:Sort ") ', ':Sort col#'],
  \ ["Move a column", 'exec "CSVMoveColumn " . quickui#input#open("source column number:", "") . " " . quickui#input#open("destination column number:", "") ', ':CSVMoveColumn source_col_# destination_col_#'],
  \ ["Sum Column", 'exec "SumCol " . quickui#input#open("Column number:", "") ', ':SumCol col_number'],
  \ ["Sum Row", 'exec "SumRow " . quickui#input#open("Row number:", "") ', ':SumRow row_number'],
  \ ], 60)


" enable to display tips in the cmdline
let g:quickui_show_tip = 1

" hit space twice to open menu
noremap <space><space> :call quickui#menu#open()<cr>
